<%-- 
    Document   : viewOwnQuiz
    Created on : Jun 12, 2022, 4:23:50 PM
    Author     : fptshop
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <title>Fail to join class</title>
        <style>
            .search-button{
                margin-left: 20px;
                border-radius: 5px;
                background-color: #0a6bff;
                color: white;
                font-size: 20px;
                height: 35px;
            }
            .search-button:hover{
                background-color: #065dd8;
            }
            .popup-with-zoom-anim{
                padding-left: 40px;
                background-color: inherit;
                color: white; 
                text-align: left;

            }
            .popup-with-zoom-anim:hover{
                background-color: #0C406F;
            }
        </style>
    </head>
    <body id="page-top" class="home">

        <!-- PAGE WRAP -->
        <div id="page-wrap">
            <!-- HEADER -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- END / HEADER -->

                <!-- PAGE CONTROL -->
                <section class="page-control">
                    <div class="container">
                        <div class="page-info" >
                            <a href="HomeController" class='link' ><i class="icon md-arrow-left"></i>Back to home</a>
                        </div>
                    </div>
                </section>
                <!-- END / PAGE CONTROL -->

                <section class="sub-banner section">
                    <div class="container">
                        <div class="row">
                            <h5 style="color: black; font-weight: bolder;margin-top: 30px;text-align: center">All Test Result</h5>
                            <table class="table" border="0" style="text-align: center; " >
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr >
                                        <td scope="col">Title</td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col">Number of test</td>

                                    </tr>
                                <c:forEach var="o" items="${listTest}">

                                    <tr>
                                        <td scope="row">${o.title}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>${numoftest}</td>
                                        <td> <a href="ViewTestResultController?quizid=${o.quizid}">View All Test</a></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

            <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->





        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script>
            if ($('.popup-with-zoom-anim').length) {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',

                    fixedContentPos: false,
                    fixedBgPos: true,

                    overflowY: 'auto',

                    closeBtnInside: true,
                    preloader: false,

                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
                $('.design-course-popup').delegate('.cancel', 'click', function (evt) {
                    evt.preventDefault();
                    $('.mfp-close').trigger('click');
                });
            }
        </script>
    </body>

</html>




