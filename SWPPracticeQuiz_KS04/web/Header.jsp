
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header id="header" class="header">
    <div class="container">

        <!-- LOGO -->
        <div class="logo"><a href="HomeController"><img src="images/logo3.png" alt=""></a></div>
        <!-- END / LOGO -->

        <!-- NAVIGATION -->
        <nav class="navigation">

            <div class="open-menu">
                <span class="item item-1"></span>
                <span class="item item-2"></span>
                <span class="item item-3"></span>
            </div>

            <!-- MENU -->
            <ul class="menu">
                <c:if test="${sessionScope.user.roleId == 1}">
                    <li class="current-menu-item"><a href="UserAdminController">Home</a></li></c:if>
                    <c:if test="${sessionScope.user.roleId == 2}">
                    <li class="current-menu-item"><a href="dashBoardQuizController">Home</a></li></c:if>
                    <c:if test="${sessionScope.user==null || sessionScope.user.roleId == 0}">
                    <li class="current-menu-item"><a href="HomeController">Home</a></li></c:if>
                    <c:if test="${sessionScope.user==null || sessionScope.user.roleId == 0}">
                    <li class="menu-item-has-children megamenu col-4"></c:if>
                        <a href="CategoryController">Category</a>
                        <ul class="sub-menu">
                        <c:forEach items = "${sessionScope.listC}" var = "o">
                            <li><a href="CategoryController?categoryId=${o.categoryId}">${o.categoryName}</a></li>
                            </c:forEach>
                    </ul>
                    <c:if test="${sessionScope.user==null}">
                    <li>
                        <a href="Login.jsp">Sign In</a>
                    </li>
                    <li>
                        <a href="register.jsp">Sign Up</a>
                    </li>
                </c:if>
                <c:if test="${sessionScope.user.roleId==0}">
                    <li class="menu-item-has-children">
                        <a href="CreateQuizController">Create Quiz</a>
                    </li>
                    <li class="menu-item-has-children megamenu col-1">
                        <a href="#">Class</a>
                        <ul class="sub-menu">
                            <li>
                                <div class="add-section">
                                    <a href="#joinClass" class="mc-btn-3 btn-style-1 popup-with-zoom-anim">Join A Class</a>
                                    <div id="joinClass" class="design-course-popup pp-add-section zoom-anim-dialog mfp-hide" style="padding:100px;">
                                        <label for="classcode" style="font-size: 40px; font-weight: bold; margin-bottom: 30px;">Enter Class Code</label>
                                        <form action="JoinClassByCode" method="post">
                                            <div class="form-item">
                                                <input style="margin-top: 5px;font-size: 30px; padding: 5px 10px; height: 50px;" type="text" name="classcode" id="classcode">
                                            </div>
                                            <div class="pp-footer">
                                                <input type="submit" style="font-size: 40px; height: 60px;" class="mc-btn-3 btn-style-1" value="Join Class">
                                                <a href="#" style="margin-top: 10px;padding: 15px 10px 18px 10px;font-size: 40px; height: 60px;" class="cancel mc-btn-3 btn-style-5">Cancel</a>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </li>
                            <!--<li><a href="#">Create Class</a></li>-->
                            <li>
                                <div class="add-section">
                                    <a href="#createClass" class="mc-btn-3 btn-style-1 popup-with-zoom-anim">Create A Class</a>
                                    <div id="createClass" class="design-course-popup pp-add-section zoom-anim-dialog mfp-hide" style="padding:100px;">
                                        <label for="className" style="font-size: 40px; font-weight: bold; margin-bottom: 25px;">Enter your Class name</label>
                                        <form action="CreateClassController" method="post">
                                            <div class="form-item">
                                                <input style="margin-top: 5px;font-size: 30px; padding: 5px 10px; height: 50px;" type="text" required minlength="1" name="classname" id="className">
                                            </div>
                                            <div class="form-item form-checkbox checkbox-style" style="padding-top: 33px;margin-right: 170px; ">
                                                <input style="color: black" id="allow" name="allow" type="checkbox" value="ON" checked="checked">
                                                <label for="allow">
                                                    <i style="width: 20px; height: 20px; font-size: 18px; margin-left: 0" class="icon-checkbox icon md-check-1"></i> 
                                                    <span style=" padding-top: 30px; color: black; font-weight: 500; font-size: 22px">Allow other people create Quiz?</span>
                                                </label>
                                            </div>
                                            <div class="pp-footer">
                                                <input type="submit" style="font-size: 40px; height: 60px;" class="mc-btn-3 btn-style-1" value="Create Class">
                                                <a href="#" style="margin-top: 10px;padding: 15px 10px 18px 10px;font-size: 40px; height: 60px;" class="cancel mc-btn-3 btn-style-5">Cancel</a>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </li>
                            <li><a href="ViewMyClassesController">My Classes</a></li>
                        </ul>
                    </c:if>
                    <c:if test="${sessionScope.user.roleId==1 || sessionScope.user.roleId==2 }">
                        <ul class="list-account-info">
                            <li class="list-item account">
                                <c:if test="${sessionScope.user.avatar == null}">
                                    <div class="account-info item-click" >
                                        <img style="background-color: white;" src="imageUpload/default-avatar.png" alt="">
                                    </div>
                                </c:if>
                                <c:if test="${sessionScope.user.avatar!=null}">
                                    <div class="account-info item-click" >
                                        <img src="imageUpload/${sessionScope.user.avatar}" alt="">
                                    </div>
                                </c:if>
                                <div class="toggle-account toggle-list">
                                    <ul class="list-account">
                                        <li><a href="ManageProfileController"><i class="icon md-config"></i>Profile</a></li>
                                        <li><a href="LogoutController"><i class="icon md-arrow-right"></i>Sign Out</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </c:if>

            </ul>
            <!-- END / MENU -->

            <!-- SEARCH BOX -->

            <!-- END / SEARCH BOX -->

            <!-- LIST ACCOUNT INFO -->
            <c:if test="${sessionScope.user.roleId==0}">
                <ul class="list-account-info">

                    <li class="list-item account">
                        <c:if test="${sessionScope.user.avatar == null}">
                            <div class="account-info item-click" >
                                <img style="background-color: white;" src="imageUpload/default-avatar.png" alt="">
                            </div>
                        </c:if>
                        <c:if test="${sessionScope.user.avatar!=null}">
                            <div class="account-info item-click" >
                                <img src="imageUpload/${sessionScope.user.avatar}" alt="">
                            </div>
                        </c:if>
                        <div class="toggle-account toggle-list">
                            <ul class="list-account">
                                <li><a href="ViewOwnQuizController"><i class="icon md-book-1"></i>Library</a></li>
                                <li><a href="ViewMyClassesController"><i class="icon md-users"></i></i>My Classes</a></li>
                                <li><a href="ManageProfileController"><i class="icon md-config"></i>Profile</a></li>
                                <li><a href="TestTitleController"><i class="icon md-shopping"></i>Test Results</a></li>
                                <li><a href="LogoutController"><i class="icon md-arrow-right"></i>Sign Out</a></li>
                            </ul>
                        </div>
                    </li>
                </c:if>

            </ul>
            <!-- END / LIST ACCOUNT INFO -->

        </nav>
        <!-- END / NAVIGATION -->
    </div>

</header>
