<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <style>
            .search-button{
                margin-left: 20px;
                border-radius: 5px;
                background-color: #0a6bff;
                color: white;
                font-size: 20px;
                height: 35px;
            }
            .search-button:hover{
                background-color: #065dd8;
            }
            .popup-with-zoom-anim{
                padding-left: 40px;
                background-color: inherit;
                color: white; 
                text-align: left;

            }
            .popup-with-zoom-anim:hover{
                background-color: #0C406F;
            }
        </style>

        <title>Test Detail</title>
    </head>
    <body id="page-top">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <div class="top-nav">

                <h4 class="sm black bold" style="font-size: 25px;">Test Result:  <span>  ${test.title}</span></h4>
                <ul >
                    <li style="list-style: none;">
                        <!--<a href="ViewQuizDetailController?quizid=${test.quizid}"><i class="icon md-close-1"></i></a>-->
                        <button onclick="history.back()" style="margin-right: 50px; float: right; font-size: 20px; font-weight: bold;margin-top: 20px; margin-bottom: 20px;height: 18px;color: black ;border: none; background-color: white; " value="Back">Back</button>
                    </li>
                </ul>

            </div>
            <section id="quizz-intro-section" class="quizz-intro-section learn-section">

                <div class="container">

                    <div class="title-ct">
                        <h2 style="font-weight: bold"></h2>

                    </div>

                    <div class="question-content-wrap">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="question-content">
                                    <h5 class="sm" style="font-size: 50px;font-weight: bold" >${test.title}</h5>
                                    <c:forEach var="o" items="${test.listQuestionTest}">

                                        <table class="table-question">
                                            <thead>
                                                <tr>
                                                    <th colspan="1" >
                                                        <span style="color: black; font-size: 20px; font-weight: bold;">Question ${o.questionid}: ${o.questionTitle}: ${o.questionStatus}</span>
                                                        <c:if test="${o.questionStatus==true}"><i class="icon icon-val md-check-2" style="color:green;"></i></c:if>
                                                        <c:if test="${o.questionStatus==false}"><i class="icon icon-err md-close-2" style="color: red;"></i></c:if>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="x" items="${o.list_optionTest}">
                                                    <tr>
                                                        <td class="td-quest options"
                                                            <c:if test="${x.optionStatus==x.rightOption && x.optionStatus==true}"> style="background-color:honeydew; color: green "</c:if>
                                                            <c:if test="${x.optionStatus!=x.rightOption && x.optionStatus==true}"> style="background-color:honeydew; color: red "</c:if>>
                                                            ${x.optionContent}
                                                        </td>
                                                        <td class="rightOption">
                                                            <c:if test="${x.rightOption==true}"><i class="icon icon-val md-check-2"style="line-height: 30px;"></i></c:if>
                                                            <c:if test="${x.rightOption==false}"><i class="icon icon-err md-close-2" style="line-height: 30px;"></i></c:if>
                                                            </td>
                                                        </tr>
                                                </c:forEach> 

                                            </tbody>
                                        </table>
                                    </c:forEach>

                                </div>
                            </div>


                            <div class="col-md-4">
                                <aside class="question-sidebar">
                                    <div class="score-sb">
                                        <h4 class="title-sb sm bold">Total score<span>${test.mark}</span></h4>
                                        <h4 class="title-sb sm bold">Correct<span>${test.correctAnswer} / ${test.listQuestionTest.size()} </span></h4>
                                        <ul>
                                            <c:forEach var="q" items="${test.listQuestionTest}">

                                                <li <c:if test="${q.questionStatus==true}"> class="active"</c:if>
                                                                                           <c:if test="${q.questionStatus==false}">class="val"</c:if>>
                                                                                               <i  class="icon" 
                                                                                                   ></i>Question ${q.questionid}
                                                </li>
                                            </c:forEach>


                                        </ul>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>

            </section>



        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script>
            if ($('.popup-with-zoom-anim').length) {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',

                    fixedContentPos: false,
                    fixedBgPos: true,

                    overflowY: 'auto',

                    closeBtnInside: true,
                    preloader: false,

                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
                $('.design-course-popup').delegate('.cancel', 'click', function (evt) {
                    evt.preventDefault();
                    $('.mfp-close').trigger('click');
                });
            }
        </script>
    </body>

</html>