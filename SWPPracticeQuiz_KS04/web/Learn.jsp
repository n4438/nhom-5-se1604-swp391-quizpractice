<%-- 
    Document   : Learn
    Created on : Jul 4, 2022, 4:10:51 PM
    Author     : HP
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">

        <title>Learn Quiz</title>
        <style>
            .rightOption{
                display: none;
            }
            .td-quest:hover{
                background-color: #eee;
            }
            .optionAfterCheck{
                background-color: red;
            }
            #skip{
                color: #139DF0; 
            }
            #skip:hover{
                color: blue; 
                background-color: #FFFFFF; 
                border: none;
            }
            #notice{
                display: none;
            }
        </style>
    </head>
    <body id="page-top">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <div class="top-nav">

                <h4 class="sm black bold" style="font-size: 25px;">Learn:  <span>  ${title}</span></h4>
                <ul class="top-nav-list">
                    <li class="backpage">
                        <a href="ViewQuizDetailController?quizid=${requestScope.quizid}"><i class="icon md-close-1"></i></a>
                    </li>
                </ul>

            </div>
            <section id="quizz-intro-section" class="quizz-intro-section learn-section">
                <div class="container">

                    <div class="title-ct">

                    </div>

                    <div class="question-content-wrap">
                        <div class="row">
                            <div class="col-md-2 title-ct">
                                <div class="question-content">
                                    <h3 style="color: black; font-size: 20px; font-weight: bold"><strong style="color: blue; font-size: 20px;">Your Score: </strong> <span id="trueAnwsers"> ${requestScope.trueAnwsers}</span> / <span id="totalQuestion"> ${requestScope.totalQuestion}</span></h3>
                                    <h3 style="color: black; font-size: 20px; font-weight: bold"><strong style="color: blue;font-size: 20px;">Cover Quiz: </strong> <span id="cover"> ${requestScope.cover} </span><span> %</span></h3>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="question-content">
                                    <h4 class="sm" style="font-weight: bold; font-size: 25px;">Question: <span> ${requestScope.question.title}</span></h4>

<!--                                    <div class="dc-quizz-info dc-course-item">
                                        <div class="quizz-body dc-item-body">
                                            <div class="form-item form-checkbox checkbox-style">
                                                <c:forEach items="${requestScope.question.listOptions}" var='o'>
                                                    <input type="checkbox" <c:if test="${o.right_option==true}"> id = "truesoption${o.optionid}" onclick="checkedOption('truesoption${o.optionid}')"</c:if>
                                                           <c:if test="${o.right_option==false}"> id = "falsesoption${o.optionid}" onclick="checkedOption('falsesoption${o.optionid}')"</c:if> name="option" value="${o.optionid}" >
                                                    <label <c:if test="${o.right_option==false}">for="falsesoption${o.optionid}"</c:if>
                                                                                                 <c:if test="${o.right_option==true}">for="truesoption${o.optionid}"</c:if>                                     >
                                                                                                     <i style="width: 18px; height: 18px; font-size: 16px;"class="icon-checkbox icon md-check-1"></i>
                                                                                                 ${requestScope.s.charAt(o.optionid-1)}.  <span> ${o.content}</span>
                                                    </label>
                                                    <br>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>-->

                                    <table class="table-question">
                                        <thead>
                                            <tr>
                                                <th colspan="2" style="font-size: 20px">Chose ${requestScope.numberOfRightOption} answer.
                                                    <span style="margin-right: 0%;float: right; color: blue;">
                                                        <!--<a href="#" style="font-size: 17px"><i class="icon md-arrow-right"></i>Skip Question</a>-->
                                                        <form action="LearnController" method="get" id="skip">
                                                            <input type="text" name="questionid" value="${requestScope.questionid}" hidden="">
                                                            <input type="text" name="totalQuestion" value="${requestScope.totalQuestion}" hidden="">
                                                            <input type="text" name="trueAnwsers" value="${requestScope.trueAnwsers}" hidden="">
                                                            <input type="text" name="skip" id="skip" value="true" hidden="">
                                                            <input type="text" name="quizid" value="${requestScope.quizid}" hidden="">
                                                            <i class="icon md-arrow-right"></i><input style="background-color: #FFFFFF; 
                                                                                                      border: none;" type="submit" value="Skip Question">
                                                        </form>
                                                    </span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <c:forEach items="${requestScope.question.listOptions}" var="o">
                                                <tr>
                                                    <td class="td-quest options" <c:if test="${o.right_option==true}"> id = "trueoption${o.optionid}" onclick="checkedOption('trueoption${o.optionid}')"</c:if>
                                                        <c:if test="${o.right_option==false}"> id = "falseoption${o.optionid}" onclick="checkedOption('falseoption${o.optionid}')"</c:if>
                                                        style="font-size: 18px;">${requestScope.s.charAt(o.optionid-1)}.  <span> ${o.content}</span></td>
                                                    <td class="rightOption">
                                                        <c:if test="${o.right_option==true}"><i class="icon icon-val md-check-2"style="line-height: 30px;"></i></c:if>
                                                        <c:if test="${o.right_option==false}"><i class="icon icon-err md-close-2" style="line-height: 30px;"></i></c:if>
                                                        </td>
                                                    </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <div id="rightAnswer">
                                        <p class="correct" style="color: green; font-weight: bold;font-size: 20px;" id="notice">
                                            Correct!
                                        </p>
                                    </div>
                                    <div class="form-action" style="font-size: 20px; font-weight: bold; color: black">
                                        <input type="submit" value="Submit" onclick="checkAnswer()" class="mc-btn btn-style-6" style="margin-left: 0px;float:left;">
                                        <span><form action="LearnController" method="get" style="float: right;">
                                                <input type="text" name="questionid" value="${requestScope.questionid}" hidden="">
                                                <input type="text" name="totalQuestion" value="${requestScope.totalQuestion}" hidden="">
                                                <input type="text" name="trueAnwsers" value="${requestScope.trueAnwsers}" hidden="">
                                                <input type="text" name="trueCurrentAnswer" id="trueCurrentAnswer" value="true" hidden="">
                                                <input type="text" name="quizid" value="${requestScope.quizid}" hidden="">
                                                <input type="submit" value="Next Question" class="mc-btn btn-style-6">
                                            </form></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>



        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="js/learnQuiz.js" type="text/javascript"></script>
    </body>

</html>