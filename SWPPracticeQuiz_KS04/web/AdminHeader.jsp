<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header id="header" class="header">
    <div class="container">

        <!-- LOGO -->
        <div class="logo"><a href="HomeController"><img src="images/logo3.png" alt=""></a></div>
        <!-- END / LOGO -->

        <!-- NAVIGATION -->
        <nav class="navigation">

            <div class="open-menu">
                <span class="item item-1"></span>
                <span class="item item-2"></span>
                <span class="item item-3"></span>
            </div>

            <!-- MENU -->
            <ul class="menu">
                <c:if test="${sessionScope.user.roleId==1}">
                    <li class="current-menu-item"><a href="UserAdminController">Home</a></li>
                    </c:if>
                    <c:if test="${sessionScope.user.roleId==2}">
                    <li class="current-menu-item"><a href="dashBoardQuizController">Home</a></li>
                    </c:if>
                <li class="menu-item-has-children megamenu col-4">
                    <c:if test="${sessionScope.user==null}">
                    <li>
                        <a href="Login.jsp">Sign In</a>
                    </li>
                    <li>
                        <a href="register.jsp">Sign Up</a>
                    </li>
                </c:if>

            </ul>
            <!-- END / MENU -->

            <!-- SEARCH BOX -->

            <!-- END / SEARCH BOX -->

            <!-- LIST ACCOUNT INFO -->
            <c:if test="${sessionScope.user.roleId==1 || sessionScope.user.roleId==2 }">
                <ul class="list-account-info">
                    <li class="list-item account">
                        <c:if test="${sessionScope.user.avatar == null}">
                            <div class="account-info item-click" >
                                <img style="background-color: white;" src="imageUpload/default-avatar.png" alt="">
                            </div>
                        </c:if>
                        <c:if test="${sessionScope.user.avatar!=null}">
                            <div class="account-info item-click" >
                                <img src="imageUpload/${sessionScope.user.avatar}" alt="">
                            </div>
                        </c:if>
                        <div class="toggle-account toggle-list">
                            <ul class="list-account">
                                <li><a href="ManageProfileController"><i class="icon md-config"></i>Profile</a></li>
                                <li><a href="LogoutController"><i class="icon md-arrow-right"></i>Sign Out</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </c:if>


            <!-- END / LIST ACCOUNT INFO -->

        </nav>
        <!-- END / NAVIGATION -->
    </div>

</header>