<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from tk-themes.net/html-megacourse/account-learning.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 May 2022 16:35:08 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <!-- Google font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- Css -->
    <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/md-font.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <title>Mega Course - Learning and Courses HTML5 Template</title>
</head>

<body id="page-top">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- PRELOADER -->
        <div id="preloader">
            <div class="pre-icon">
                <div class="pre-item pre-item-1"></div>
                <div class="pre-item pre-item-2"></div>
                <div class="pre-item pre-item-3"></div>
                <div class="pre-item pre-item-4"></div>
            </div>
        </div>
        <!-- END / PRELOADER -->

        <!-- HEADER -->
        <jsp:include page="Header.jsp"></jsp:include>    


        <!-- PROFILE FEATURE -->
        <section class="profile-feature">
            <div class="awe-parallax bg-profile-feature"></div>
            <div class="awe-overlay overlay-color-3"></div>
            <div class="container">
                <div class="info-author">
                    <div class="image">
                        <img src="images/team-13.jpg" alt="">
                    </div>
                    <div class="name-author">
                        <h2 class="big">${sessionScope.user.getUserName()}</h2>
                    </div>
                    <div class="address-author">
                        <i class="fa fa-map-marker"></i>
                        <h3>${sessionScope.user.getEmail()}</h3><br>
                    </div>
                </div>
                <div class="info-follow">
                    <div class="trophies">
                        <span>12</span>
                        <p>Member</p>
                    </div>
                    <div class="trophies">
                        <span>20</span>
                        <p>Total Quiz</p>
                    </div>
                    <div class="trophies">
                        <span>$ 149,902</span>
                        <p>Your created Quiz</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / PROFILE FEATURE -->


        <!-- CONTEN BAR -->
        <section class="content-bar">
            <div class="container">
                <ul>
                    <li class="current">
                        <a href="account-learning.html">
                            <i class="icon md-book-1"></i>
                            Other people's quiz
                        </a>
                    </li>
                    <li>
                        <a href="account-teaching.html">
                            <i class="icon md-people"></i>
                            Your created quiz
                        </a>
                    </li>
                    <li>
                        <a href="account-assignment.html">
                            <i class="icon md-shopping"></i>
                            Member in class
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <!-- END / CONTENT BAR -->

        <!-- COURSE CONCERN -->
        <section  id="course-concern" class="course-concern">
            <!-- SEARCH BOX -->
            <!-- <div class="search-box">
                <i class="icon md-search"></i>
                <div class="search-inner">
                    <form>
                        <input type="text" placeholder="key words">
                    </form>
                </div>
            </div> -->
            <!-- END / SEARCH BOX -->
            <!-- SUB BANNER RESULT NOT FOUND -->
            <div style="margin-left: 47px; font-size: 20px; font-weight: 600">
                Number Of Quiz ()
            </div>
            <section  class="sub-banner-search">
                <div class="container">
                    <div class="sub-banner-content-result">
                        <div class="form-item">
                            <input type="text">
                        </div>
                        <div class="form-actions">
                            <input type="submit" value="Search">
                        </div>
                    </div>
                </div>
            </section>

            <!-- END / SUB BANNER RESULT NOT FOUND -->
            <div class="container" style="margin-top: 17px">
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">100%<i class="fa fa-trophy"></i></div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">75%</div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">100%<i class="fa fa-trophy"></i></div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">0%</div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">90%</div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">45%</div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">30%</div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="images/feature/img-1.jpg" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">Web design</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="course-intro.html">Advanced Tactics with Hoot Suite Pro</a></h4>
                                <div class="name-author">
                                    By <a href="#">Name of Mr or Mrs</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">10%</div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>

                </div>
            </div>
        </section>
        <!-- END / COURSE CONCERN -->



        <!-- FOOTER -->
        <footer id="footer" class="footer">
            <div class="first-footer">
                <div class="container">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="widget megacourse">
                                <h3 class="md">MegaCourse</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
                                <a href="#" class="mc-btn btn-style-1">Get started</a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget widget_latest_new">
                                <h3 class="sm">Latest News</h3>
                                <ul>
                                    <li>
                                        <a href="#">
                                            <div class="image-thumb">
                                                <img src="images/team-13.jpg" alt="">
                                            </div>
                                            <span>How to have a good Boyfriend in college?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="image-thumb">
                                                <img src="images/team-13.jpg" alt="">
                                            </div>
                                            <span>How to have a good Boyfriend in college?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="widget quick_link">
                                <h3 class="sm">Quick Links</h3>
                                <ul class="list-style-block">
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Terms of use</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="widget news_letter">
                                <div class="awe-static bg-news_letter"></div>
                                <div class="overlay-color-3"></div>
                                <div class="inner">
                                    <div class="letter-heading">
                                        <h3 class="md">News letter</h3>
                                        <p>Don’t miss a course sale! Join our network today and keep it up!</p>
                                    </div>
                                    <div class="letter">
                                        <form>
                                            <input class="input-text" type="text">
                                            <span class="no-spam">* No spam guaranteed</span>
                                            <input type="submit" value="Submit now" class="mc-btn btn-style-2">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="second-footer">
                <div class="container">
                    <div class="contact">
                        <div class="email">
                            <i class="icon md-email"></i>
                            <a href="#">course@megadrupal.com</a>
                        </div>
                        <div class="phone">
                            <i class="fa fa-mobile"></i>
                            <span>+84 989 999 888</span>
                        </div>
                        <div class="address">
                            <i class="fa fa-map-marker"></i>
                            <span>Maecenas sodales, nisl eget</span>
                        </div>
                    </div>
                    <p class="copyright">Copyright © 2014 Megadrupal. All rights reserved.</p>
                </div>
            </div>
        </footer>
        <!-- END / FOOTER -->






    </div>
    <!-- END / PAGE WRAP -->

    <!-- Load jQuery -->
    <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
    <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
</body>

<!-- Mirrored from tk-themes.net/html-megacourse/account-learning.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 May 2022 16:35:08 GMT -->

</html>