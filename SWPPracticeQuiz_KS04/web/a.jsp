<%-- 
    Document   : Test
    Created on : Jun 21, 2022, 4:20:31 PM
    Author     : dangm
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from tk-themes.net/html-megacourse/quizz-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 May 2022 16:35:11 GMT -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <title>Mega Course - Learning and Courses HTML5 Template</title>
        <script>
            function startTimer(duration, display) {
                var timer = duration, minutes, seconds;
                setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    display.textContent = minutes + ":" + seconds;

                    if (--timer < 0) {
                        timer = duration;
                    }
                }, 1000);
            }

            window.onload = function () {
                var time = 60 * ${minute},
                        display = document.querySelector('#time');
                startTimer(time, display);
            };
        </script>
    </head>
    <body id="page-top">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <div class="top-nav">

                <h4 class="sm black bold"><a href="HomeController" style="color: #0f649d">Home</a>/take test</h4>

                <ul class="top-nav-list">
                    <li class="prev-course"><a href="#"><i class="icon md-angle-left"></i><span class="tooltip">Prev</span></a></li>
                    <li class="next-course"><a href="#"><i class="icon md-angle-right"></i><span class="tooltip">Next</span></a></li>
                    <li class="backpage">
                        <a href="ViewQuizDetailController?quizid=${quizid}"><i class="icon md-close-1"></i></a>
                    </li>
                </ul>

            </div>
            <section id="quizz-intro-section" class="quizz-intro-section learn-section">
                <div class="container">

                    <div class="title-ct">
                        <h3><strong>Quizz ${quizid}</strong>${titleQuiz}</h3>
                        <div class="tt-right">
                            <a href="#" class="skip"><i class="icon md-arrow-right"></i>Skip quizz</a>
                        </div>
                    </div>

                    <div class="question-content-wrap">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="question-content">
                                    <h4 class="sm">Question 2 - Multiple corrects</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                                    <div class="answer">
                                        <h4 class="sm">Answer</h4>
                                        <ul class="answer-list">
                                            <li>
                                                <input type="checkbox" name="checkbox-1" id="checkbox-1">
                                                <label for="checkbox-1">
                                                    <i class="icon icon_check md-check-1"></i>
                                                    sed diam nonummy nibh euismod tincidunt ut laoreet
                                                </label>
                                            </li>
                                            <li>
                                                <input type="checkbox" name="checkbox-1" id="checkbox-2">
                                                <label for="checkbox-2">
                                                    <i class="icon icon_check md-check-1"></i>
                                                    ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequa
                                                </label>
                                            </li>
                                            <li>
                                                <input type="checkbox" name="checkbox-1" id="checkbox-3">
                                                <label for="checkbox-3">
                                                    <i class="icon icon_check md-check-1"></i>
                                                    ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequa
                                                </label>
                                            </li>
                                        </ul>

                                    </div>
                                    <a href="#" class="mc-btn btn-style-6">Previous Question</a>
                                    <a href="#" class="mc-btn btn-style-6">Next question</a>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <aside class="question-sidebar">
                                    <div class="score-sb">
                                        <h4 class="title-sb sm bold sidebar-heading"><div>Time <span id="time"></span> </div></h4>

                                            
                                            <ul>
                                                <c:forEach item="${requestScope.ListQuestionInTest}" var="o"> 
                                                <li class="val"><i class="icon"></i>Question ${o.questionid} </li>
                                                </c:forEach>
                                                ${ListQuestionInTest.size()}
                                                <c:forEach items="${listCategory}" var='o'>
                                                                        <input type="checkbox" id='${o.categoryId}' name="category" value="${o.categoryId}"
                                                                               <c:if test="${o.cateOfQuiz==true}">checked</c:if>
                                                                               >
                                                                        <label for="${o.categoryId}">
                                                                            <i style="width: 18px; height: 18px; font-size: 16px;"class="icon-checkbox icon md-check-1"></i>
                                                                            ${o.categoryName}
                                                                        </label>
                                                                    </c:forEach>
                                            </ul>
                                        
                                    </div>
                                    <input type="submit" value="Submit all answer" class="submit mc-btn btn-style-1">
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
        <!-- END / PAGE WRAP -->
        <script>
            ${numOfQues}.forEach(myFunction)
            document.getElementById("index").innerHTML = i;
            function index(NumberOfQuiz) {
                for(i=1;i<=NumberOfQuiz;i++){
                    i;
                }
            }
        </script>
        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="assets/vendor/jquery.countdown.min.js"></script>

        <!-- Init -->
        <script src="assets/js/countdown.js"></script>
    </body>

    <!-- Mirrored from tk-themes.net/html-megacourse/quizz-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 May 2022 16:35:11 GMT -->
</html>
