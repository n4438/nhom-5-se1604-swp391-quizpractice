package Validation;

import java.util.Properties;
import java.util.Random;

import java.math.BigDecimal;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Utilities {

    public static boolean sendEmail(String toEmail, String Subject, String msg) {
        boolean test = false;
        String fromEmail = "QuizPracticeSWP391@gmail.com";
//        String password = "Quiz11111";
        String password = "gjfuyrhepoicbjcd";
        try {
            Properties props = new Properties();
            // props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
            props.put("mail.smtp.port", "587"); // TLS Port
            props.put("mail.smtp.auth", "true"); // enable authentication
            props.put("mail.smtp.starttls.enable", "true"); // enable STARTTLS
            Session session = Session.getInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromEmail, password);
                }
            });
            MimeMessage mess = new MimeMessage(session);
            mess.setFrom(new InternetAddress(fromEmail));
            mess.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            mess.setSubject(Subject);
            mess.setContent(msg, "text/html");
            Transport.send(mess);
            test = true;

        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return test;
    }

    public static String generateRandomCode() {
        String code = "";
        String s = "abcdefghijklmnopqrstuwxyzABCDYFGHIJKLMNOPQRSTUWXYZ1234567890";
        int charAt;
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            charAt = random.nextInt(60);
            code += s.charAt(charAt);
        }
        return code;
    }

    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    public static void main(String[] args) {
        sendEmail("thuongdthe150682@gmail.com", "test", "1223");
    }

}
