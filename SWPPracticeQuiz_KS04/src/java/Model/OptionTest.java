/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author dangm
 */
public class OptionTest {
    private int testid;
    private int questionid;
    private int optionid;
    private String optionContent;
    private boolean rightOption;
    private boolean optionStatus;

    public OptionTest() {
    }

    public OptionTest(int testid, int questionid, int optionid, String optionContent, boolean rightOption, boolean optionStatus) {
        this.testid = testid;
        this.questionid = questionid;
        this.optionid = optionid;
        this.optionContent = optionContent;
        this.rightOption = rightOption;
        this.optionStatus = optionStatus;
    }
    

    public OptionTest(int testid, int questionid, int optionid, String optionContent, boolean rightOption) {
        this.testid = testid;
        this.questionid = questionid;
        this.optionid = optionid;
        this.optionContent = optionContent;
        this.rightOption = rightOption;
    }
    
    

    public int getTestid() {
        return testid;
    }

    public void setTestid(int testid) {
        this.testid = testid;
    }

    public int getQuestionid() {
        return questionid;
    }

    public void setQuestionid(int questionid) {
        this.questionid = questionid;
    }

    public int getOptionid() {
        return optionid;
    }

    public void setOptionid(int optionid) {
        this.optionid = optionid;
    }

    public String getOptionContent() {
        return optionContent;
    }

    public void setOptionContent(String optionContent) {
        this.optionContent = optionContent;
    }

    public boolean isOptionStatus() {
        return optionStatus;
    }

    public void setOptionStatus(boolean optionStatus) {
        this.optionStatus = optionStatus;
    }
    


    public boolean isRightOption() {
        return rightOption;
    }

    public void setRightOption(boolean rightOption) {
        this.rightOption = rightOption;
    }


    @Override
    public String toString() {
        return "OptionTest{" + "testid=" + testid + ", questionid=" + questionid + ", optionid=" + optionid + ", optionContent=" + optionContent + ", rightOption=" + rightOption + ", optionStatus=" + optionStatus + '}';
    }
    
    
}
