/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author dangm
 */
public class Test {
    private int testid, userid, quizid;
    private String title, decription;
    private Date dateCreated, lastUpdate;
    private int numberOfQuestion;
    private float mark;
    private String time;
    private int correctAnswer;
    private List<QuestionTest> listQuestionTest;
    public Test() {
    }

    public Test(int testid, int userid, int quizid, String title, String decription, Date dateCreated, Date lastUpdate, int numberOfQuestion, float mark, String time, int correctAnswer, List<QuestionTest> listQuestionTest) {
        this.testid = testid;
        this.userid = userid;
        this.quizid = quizid;
        this.title = title;
        this.decription = decription;
        this.dateCreated = dateCreated;
        this.lastUpdate = lastUpdate;
        this.numberOfQuestion = numberOfQuestion;
        this.mark = mark;
        this.time = time;
        this.correctAnswer = correctAnswer;
        this.listQuestionTest = listQuestionTest;
    }



    public List<QuestionTest> getListQuestionTest() {
        return listQuestionTest;
    }

    public void setListQuestionTest(List<QuestionTest> listQuestionTest) {
        this.listQuestionTest = listQuestionTest;
    }
    
    public int getTestid() {
        return testid;
    }

    public void setTestid(int testid) {
        this.testid = testid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getQuizid() {
        return quizid;
    }

    public void setQuizid(int quizid) {
        this.quizid = quizid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }



    public int getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public void setNumberOfQuestion(int numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public String toString() {
        return "Test{" + "testid=" + testid + ", userid=" + userid + ", quizid=" + quizid + ", title=" + title + ", decription=" + decription + ", dateCreated=" + dateCreated + ", lastUpdate=" + lastUpdate + ", numberOfQuestion=" + numberOfQuestion + ", mark=" + mark + ", time=" + time + ", correctAnswer=" + correctAnswer + '}';
    }
    
}
