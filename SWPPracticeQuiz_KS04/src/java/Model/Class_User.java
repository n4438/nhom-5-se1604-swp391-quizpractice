
package Model;

import java.sql.Date;


public class Class_User {
    private int classid;
    private int userid;
    private Date dateJoin;
    private boolean isAdmin;
    private int joinBy;
    private String username;
    private String usernameAdd;
    public Class_User() {
    }

    public Class_User(int classid, int userid, Date dateJoin, boolean isAdmin, int joinBy) {
        this.classid = classid;
        this.userid = userid;
        this.dateJoin = dateJoin;
        this.isAdmin = isAdmin;
        this.joinBy = joinBy;
    }

    public Class_User(int classid, int userid, Date dateJoin, boolean isAdmin, int joinBy, String username) {
        this.classid = classid;
        this.userid = userid;
        this.dateJoin = dateJoin;
        this.isAdmin = isAdmin;
        this.joinBy = joinBy;
        this.username = username;
    }
    
    public Class_User(int classid, int userid, Date dateJoin, boolean isAdmin, int joinBy, String username,String usernameAdd) {
        this.classid = classid;
        this.userid = userid;
        this.dateJoin = dateJoin;
        this.isAdmin = isAdmin;
        this.joinBy = joinBy;
        this.username = username;
        this.usernameAdd = usernameAdd;
    }
    
    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Date getDateJoin() {
        return dateJoin;
    }

    public void setDateJoin(Date dateJoin) {
        this.dateJoin = dateJoin;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getJoinBy() {
        return joinBy;
    }

    public void setJoinBy(int joinBy) {
        this.joinBy = joinBy;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameAdd() {
        return usernameAdd;
    }

    public void setUsernameAdd(String usernameAdd) {
        this.usernameAdd = usernameAdd;
    }
    
    
    @Override
    public String toString() {
        return "Class_User{" + "classid=" + classid + ", userid=" + userid + ", dateJoin=" + dateJoin + ", isAdmin=" + isAdmin + ", joinBy=" + joinBy + '}';
    }
    
}
