/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author MSI
 */
public class TestTitle {
    private String title;
    private int userid,quizid;

    public TestTitle() {
    }

    public TestTitle(String title, int userid, int quizid) {
        this.title = title;
        this.userid = userid;
        this.quizid = quizid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getQuizid() {
        return quizid;
    }

    public void setQuizid(int quizid) {
        this.quizid = quizid;
    }

    @Override
    public String toString() {
        return "TestTitle{" + "title=" + title + ", userid=" + userid + ", quizid=" + quizid + '}';
    }
    
}
