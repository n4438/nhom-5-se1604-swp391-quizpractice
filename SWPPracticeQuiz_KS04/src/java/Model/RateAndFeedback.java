package Model;
public class RateAndFeedback {
    private int rateid;
    private int userid;
    private int quizid;
    private int rate;
    private String feedback;
    private String username;
    private String avatar;
    public RateAndFeedback() {
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    public RateAndFeedback(int userid, int quizid, int rate, String feedback) {
        this.userid = userid;
        this.quizid = quizid;
        this.rate = rate;
        this.feedback = feedback;
    }

    public RateAndFeedback(int userid, int quizid, int rate, String feedback, String username) {
        this.userid = userid;
        this.quizid = quizid;
        this.rate = rate;
        this.feedback = feedback;
        this.username = username;
    }

    public RateAndFeedback(int userid, int quizid, int rate, String feedback, String username, String avatar) {
        this.userid = userid;
        this.quizid = quizid;
        this.rate = rate;
        this.feedback = feedback;
        this.username = username;
        this.avatar = avatar;
    }
    
    

    
 
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public int getRateid() {
        return rateid;
    }

    public void setRateid(int rateid) {
        this.rateid = rateid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getQuizid() {
        return quizid;
    }

    public void setQuizid(int quizid) {
        this.quizid = quizid;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override

    public String toString() {
        return "RateAndFeedback{" + "rateid=" + rateid + ", userid=" + userid + ", quizid=" + quizid + ", rate=" + rate + ", feedback=" + feedback + ", username=" + username + '}';
    }
    

    
}
