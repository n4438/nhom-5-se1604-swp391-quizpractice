package Controller;

import DAO.ClassDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "DeleteAClassController", urlPatterns = {"/DeleteAClassController"})
public class DeleteAClassController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            request.setAttribute("message", "You need to login before delete a class.");
        } else {
            String classid = request.getParameter("classid");
            ClassDAO classdao = new ClassDAO();
            boolean check = classdao.checkUserIsCreator(classid, user.getId());
            if (check) {
                check = classdao.deleteAClassById(classid);
                if (check) {
                    request.setAttribute("message", "Delete Class Successfully.");
                    request.getRequestDispatcher("ViewMyClassesController").forward(request, response);
                } else {
                    request.setAttribute("message", "Delete Class Fail. Please Check again.");
                    request.getRequestDispatcher("ViewMyClassesController").forward(request, response);
                }
            } else {
                request.setAttribute("message", "You're not allowed to delete this class.");
                request.getRequestDispatcher("ViewMyClassesController").forward(request, response);
            }

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DeleteAClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DeleteAClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
