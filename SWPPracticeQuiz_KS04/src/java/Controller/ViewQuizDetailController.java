/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.QuizDAO;
import DAO.RateAndFeedBackDAO;
import Model.Question;
import Model.Quiz;
import Model.RateAndFeedback;
import DAO.TestDAO;
import Model.Question;
import Model.Quiz;
import Model.Test_Load;
import Model.User;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fptshop
 */
public class ViewQuizDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String xQuizid = request.getParameter("quizid");
        String currentIndex = request.getParameter("currentIndex");
        RateAndFeedback rate = (RateAndFeedback) request.getAttribute("rate");
        String feedbackEdit = request.getParameter("feedbackEdit");
        String ratingEdit = request.getParameter("ratingEdit");
        if (user != null) {
            QuizDAO dao = new QuizDAO();
            dao.addQuizView(user.getId(), Integer.parseInt(xQuizid));
            Quiz quiz = dao.getQuizDetails(xQuizid);
            RateAndFeedBackDAO dao2 = new RateAndFeedBackDAO();
            if(ratingEdit!=null && ratingEdit!=null){
                dao2.editRateAndFeedback(user.getId(), Integer.parseInt(xQuizid), Integer.parseInt(ratingEdit), feedbackEdit);
            }
            int userIdOfQuizId = dao2.getUserIdByQuizId(Integer.parseInt(xQuizid));
            RateAndFeedback check = dao2.hasRateTheQuiz(user.getId(), Integer.parseInt(xQuizid));
            List<RateAndFeedback> Top3RateAndFeedbacks = dao2.getTop3RateOfQuiz(Integer.parseInt(xQuizid),0);
            List<RateAndFeedback> listRateAndFeedbacks = dao2.getListRateOfQuiz(Integer.parseInt(xQuizid));
            // get average rating of Quiz
            int total_rate = 0;
            for(int i=0; i<listRateAndFeedbacks.size(); i++){
                total_rate+=listRateAndFeedbacks.get(i).getRate();
            }
            double average_rate = (double) Math.round((double)total_rate/listRateAndFeedbacks.size() * 10) / 10;
            List<Test_Load> listTest = new TestDAO().getAllTestByQuizIdAndUserId(Integer.parseInt(xQuizid),user.getId());
            if (currentIndex == null) {
                currentIndex = "1";
            } else if (Integer.parseInt(currentIndex) < 1) {
                currentIndex = String.valueOf(quiz.getListQuestions().size());
            } else if (Integer.parseInt(currentIndex) > quiz.getListQuestions().size()) {
                currentIndex = "1";
            }
            int totalNumberOfQuiz = quiz.getListQuestions().size();
            Question question = dao.getQuestionDetails(xQuizid, currentIndex);
            request.setAttribute("quizid", quiz.getQuizid());
            String s = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
            if(listTest.isEmpty()){
                request.setAttribute("noti", "You haven't taken any test yet");
            }else{
                request.setAttribute("listTest", listTest);
            }
            request.setAttribute("s", s);
            request.setAttribute("totalNumberOfQuiz", totalNumberOfQuiz);
            request.setAttribute("quizid", xQuizid);
            request.setAttribute("currentIndex", currentIndex);
            request.setAttribute("userIdOfQuizId", userIdOfQuizId);
            request.setAttribute("question", question);
            request.setAttribute("quiz", quiz);
            request.setAttribute("rate", rate);
            request.setAttribute("check", check);
            request.setAttribute("user", user);
            request.setAttribute("listRateAndFeedbacks", listRateAndFeedbacks);
            request.setAttribute("Top3RateAndFeedbacks", Top3RateAndFeedbacks);
            request.setAttribute("average_rate", average_rate);
            request.getRequestDispatcher("viewQuizDetail.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewQuizDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewQuizDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
