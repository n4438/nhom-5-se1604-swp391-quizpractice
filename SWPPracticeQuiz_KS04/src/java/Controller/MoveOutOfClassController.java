
package Controller;

import DAO.ClassDAO;
import Model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "MoveOutOfClassController", urlPatterns = {"/MoveOutOfClassController"})
public class MoveOutOfClassController extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String classid = request.getParameter("classid");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            request.setAttribute("message", "You need to login before delete a member. ");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        } else {
            ClassDAO classdao = new ClassDAO();
            int classidd = Integer.parseInt(classid);
//            boolean checkIfUserCreated = classdao.checkUserIsCreator(classid,user.getId());
            
            boolean check = classdao.leaveClass(user.getId(), classidd);
            if (check) {
                request.setAttribute("message", "You're leave class successfully.");
                request.getRequestDispatcher("HomeController").forward(request, response);
//            request.getRequestDispatcher("ViewMyClassController").forward(request, response);
            } else {
                request.setAttribute("classid", classid);
                request.setAttribute("messageDelete2", "Error to move out class. Please check again.");
                request.getRequestDispatcher("ViewQuizInClassController").forward(request, response);
            }
            
        }

    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(MoveOutOfClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(MoveOutOfClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
