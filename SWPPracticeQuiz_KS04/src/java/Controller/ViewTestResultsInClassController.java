
package Controller;

import DAO.TestDAO;
import Model.TestTitle;
import Model.Test_Load;
import Model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "ViewTestResultsInClassController", urlPatterns = {"/ViewTestResultsInClassController"})
public class ViewTestResultsInClassController extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        TestDAO dao = new TestDAO();
        HttpSession s = request.getSession();
        User user = (User) s.getAttribute("user");
        int id = user.getId();
        String classid = request.getParameter("classid");
        List<TestTitle> listTest = dao.getAllTestByUserIdInClass(classid,id);
        int numoftest = listTest.size();
        request.setAttribute("listTest", listTest);
        request.setAttribute("numoftest", numoftest);
        request.getRequestDispatcher("viewTestTitle.jsp").forward(request, response);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewTestResultsInClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewTestResultsInClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
