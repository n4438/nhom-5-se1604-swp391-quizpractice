package Controller;

import DAO.TestDAO;
import Model.OptionTest;
import Model.Test;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SubmitTestController", urlPatterns = {"/SubmitTestController"})
public class SubmitTestController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String testid = request.getParameter("testid");
        TestDAO testdao = new TestDAO();
        Test test = testdao.getTestDetails(testid);
        boolean check;
        float count = 0;
        for (int i = 0; i < test.getListQuestionTest().size(); i++) {
            check = true;
            for (OptionTest list_optionTest : test.getListQuestionTest().get(i).getList_optionTest()) {
                if (list_optionTest.isRightOption() != list_optionTest.isOptionStatus()) {
                    check = false;
                    break;
                }
            }
            testdao.updateQuestionForSubmitTest(testid, test.getListQuestionTest().get(i).getQuestionid()+"", check);
            if(check){
                count++;
            }
        }
        float mark =(float) (count/test.getListQuestionTest().size())*10;
        System.out.println(mark);
        testdao.updateTestForSubmitTest(mark,count,testid);
        request.setAttribute("testid", testid);
        request.getRequestDispatcher("ViewTestResultDetailController").forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SubmitTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SubmitTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
