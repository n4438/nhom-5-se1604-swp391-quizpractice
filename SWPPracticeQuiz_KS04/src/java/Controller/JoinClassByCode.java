
package Controller;

import DAO.ClassDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "JoinClassByCode", urlPatterns = {"/JoinClassByCode"})
public class JoinClassByCode extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        // code of class 1 : 0111122022th
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user==null){
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }else{
            String classcode = request.getParameter("classcode");
            ClassDAO classdao = new ClassDAO();
            if(!classdao.checkClassExistByCode(classcode)){
                request.getRequestDispatcher("ClassError.jsp").forward(request, response);
            }else{
                int classid = classdao.getClassIdByCode(classcode);
                if(classdao.checkIfUserInClass(classid, user.getId())){
                    request.setAttribute("message", "You were in this class");
                    request.setAttribute("classid", classid);
                    request.getRequestDispatcher("ViewQuizInClassController").forward(request, response);
                }else{
                    boolean check = classdao.joinClassByCode(classid, user.getId());
                    if(check){
                        request.setAttribute("message", "Welcome!");
                        request.setAttribute("classid", classid);
                        request.getRequestDispatcher("ViewQuizInClassController").forward(request, response);
                    }else{
                        request.setAttribute("message", "or You are not allow to join the class.");
                        request.getRequestDispatcher("ClassError.jsp").forward(request, response);
                    }
                }
                
                
            }
        
        }
        
      
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(JoinClassByCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(JoinClassByCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
