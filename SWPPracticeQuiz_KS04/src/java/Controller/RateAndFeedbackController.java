/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RateAndFeedBackDAO;
import Model.RateAndFeedback;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet(name = "RateAndFeedbackController", urlPatterns = {"/RateAndFeedbackController"})
public class RateAndFeedbackController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String quizid = request.getParameter("quizid");
        String rating1 = request.getParameter("rating1");
        String feedback = request.getParameter("feedback");
        RateAndFeedback rate = new RateAndFeedback(user.getId(), Integer.parseInt(quizid), Integer.parseInt(rating1), feedback);
        if (user != null) {
            RateAndFeedBackDAO dao = new RateAndFeedBackDAO();
            dao.addRateAndFeedBack(user.getId(), Integer.parseInt(quizid), Integer.parseInt(rating1), feedback);
            request.setAttribute("rate", rate);
            request.getRequestDispatcher("ViewQuizDetailController").forward(request, response);
        } else {
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(RateAndFeedbackController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(RateAndFeedbackController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
