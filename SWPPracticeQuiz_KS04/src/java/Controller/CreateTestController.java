/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.QuizDAO;
import DAO.TestDAO;
import Model.Question;
import Model.Quiz;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dangm
 */
@WebServlet(name = "CreateTestController", urlPatterns = {"/CreateTestController"})
public class CreateTestController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
//            String questionid = request.getParameter("questionid");
//            if(questionid==null || questionid.equals("")){
//                questionid="1";
//            }
//            String questionid = "1";
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            int quizid =Integer.parseInt(request.getParameter("quizid"));
            int totalNumberOfQuiz =Integer.parseInt(request.getParameter("totalNumberOfQuiz"));
            //int hour = Integer.parseInt(request.getParameter("hour"));
            double minute = Double.parseDouble(request.getParameter("minute"));
            QuizDAO quizDAO = new QuizDAO();
            Quiz quiz = quizDAO.getQuizDetails(String.valueOf(quizid));
            TestDAO testDao = new TestDAO();
            boolean checkCreateTest = testDao.CreateNewTest(user.getId(),quizid,quiz.getTitle(),quiz.getDescription(),totalNumberOfQuiz,minute);
            int testid = 0;
            if(checkCreateTest){
                testid = testDao.getLastTestByUserID(user.getId());
            }
            List<Question> ListQuestionInTest = testDao.getQuestionForTest(quizid, totalNumberOfQuiz);
            boolean check ; 
            for (int i = 0; i < ListQuestionInTest.size(); i++) {
                int count = testDao.countNumberOfRightAnswer(ListQuestionInTest.get(i));
                check = testDao.insertNewQuestionTest(ListQuestionInTest.get(i).getQuestionid(), testid, ListQuestionInTest.get(i).getTitle(), ListQuestionInTest.get(i).getIntruction(), count);
                for (int j = 0; j < ListQuestionInTest.get(i).getListOptions().size(); j++) {
                    testDao.insertNewOptionTest(testid, ListQuestionInTest.get(i).getQuestionid(), ListQuestionInTest.get(i).getListOptions().get(j).getOptionid(), ListQuestionInTest.get(i).getListOptions().get(j).getContent(), ListQuestionInTest.get(i).getListOptions().get(j).isRight_option());
                }
            }
            //Question question = ListQuestionInTest.get(0);
            QuizDAO Qdao = new QuizDAO();
            //String titleQuiz = Qdao.getTitleByQuizId(quizid);
//            request.setAttribute("hour", hour);
            request.setAttribute("minute", minute);
            request.setAttribute("testid", testid+"");
//            request.setAttribute("quizid", quizid);
            //request.setAttribute("ListQuestionInTest", ListQuestionInTest);
            //request.setAttribute("question", question);
            //request.setAttribute("questionid", 0);
            //request.setAttribute("titleQuiz", titleQuiz);
//            request.getRequestDispatcher("takeTest.jsp").forward(request, response);
            request.getRequestDispatcher("TakeTestController").forward(request, response);
            //request.getRequestDispatcher("index.html").forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(CreateTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(CreateTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
