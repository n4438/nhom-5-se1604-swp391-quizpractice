package Controller;

import DAO.CategoryDAO;
import DAO.QuizDAO;
import DAO.UserAdminDAO;
import Model.Category;
import Model.QuizUser;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "HomeController", urlPatterns = {"/HomeController"})
public class HomeController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        CategoryDAO dao = new CategoryDAO();
        if (session.isNew()) {
            UserAdminDAO UAdao = new UserAdminDAO();
            String dateNow = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            if (UAdao.checkVisit(dateNow)) {
                UAdao.updateVisitor(dateNow);
            } else {
                UAdao.insertVisitor();
            }
        }
        List<Category> listC = dao.getAllCategory();
        QuizDAO qDao = new QuizDAO();
        String categoryId = request.getParameter("categoryId");
//        List<QuizUser> listQuiz = qDao.getAllQuiz();
//        request.setAttribute("listC", listC);
        session.setAttribute("listC", listC);
        // get parameter index
        String indexPage = request.getParameter("index");
        // get txt search
        String txtSearch = request.getParameter("txtSearch");
        // check text search null or not
        if (txtSearch == null) {
            txtSearch = "";
        }
        int endPage;
        System.out.println("+" + indexPage + "+");
        // check index page null or not
        if (indexPage == null) {
            indexPage = "1";
        }
        List<QuizUser> listQuiz;
            listQuiz = qDao.getAllQuizByPage(indexPage, txtSearch);
            endPage = qDao.countAllQuizSearch(txtSearch);
        if (endPage % 6 == 0) {
            endPage = endPage / 6;
        } else {
            endPage = endPage / 6 + 1;
        }
        request.setAttribute("txtSearch", txtSearch);
        request.setAttribute("endPage", endPage);
        request.setAttribute("index", indexPage);
        request.setAttribute("listQuiz", listQuiz);
        request.getRequestDispatcher("Home.jsp").forward(request, response);
       
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
