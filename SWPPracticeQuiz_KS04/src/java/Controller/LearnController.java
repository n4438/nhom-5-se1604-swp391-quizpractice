package Controller;

import DAO.QuizDAO;
import DAO.QuizLearningDAO;
import Model.Question;
import Model.Quiz;
import Validation.Utilities;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LearnController", urlPatterns = {"/LearnController"})
public class LearnController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String quizid = request.getParameter("quizid");
        String questionid = request.getParameter("questionid");
        String totalQuestion = request.getParameter("totalQuestion");
        String trueAnwsers = request.getParameter("trueAnwsers");
        String trueCurrentAnswer = request.getParameter("trueCurrentAnswer");
        String skip = request.getParameter("skip");
        if (skip == null || skip.equals("")) {
            skip = "false";
        }
        // set total questions done
        if (totalQuestion == null) {
            totalQuestion = "0";
        } else if (totalQuestion.equals("")) {
            totalQuestion = "0";
        } else {
            int i = Integer.parseInt(totalQuestion);
            if (!skip.equals("true")) {
                i++;
                totalQuestion = i + "";
            }

        }
        // set true/false current anwers
        if (trueCurrentAnswer == null) {
            trueCurrentAnswer = "false";
        } else if (trueCurrentAnswer.equals("")) {
            trueCurrentAnswer = "false";
        }

        //set number of true answers done
        if (trueAnwsers == null || trueAnwsers.equals("")) {
            trueAnwsers = "0";
        } else {
            int i = Integer.parseInt(trueAnwsers);
            if (trueCurrentAnswer.equals("true")) {
                i++;
            }
            trueAnwsers = i + "";
        }
        int numOfQuestion = 0;
        if (null == questionid) {
            questionid = "0";
        } else {
            switch (questionid) {
                case "":
                    questionid = "0";
                    break;
                default:
                    numOfQuestion = Integer.parseInt(questionid) + 1;
                    questionid = numOfQuestion + "";
                    break;
            }
        }
        QuizDAO dao = new QuizDAO();
        QuizLearningDAO ldao = new QuizLearningDAO();
        Quiz quiz = dao.getQuizDetails(quizid);
        if (numOfQuestion >= quiz.getListQuestions().size()) {
            numOfQuestion = 0;
            questionid = "0";
        }
        Question question = quiz.getListQuestions().get(numOfQuestion);
        int numberOfRightOption = ldao.countRightOption(question);
        String s = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        String cover = "0.0";
        if (!totalQuestion.equals("0")) {
            float total = Float.parseFloat(totalQuestion);
            float trueAns = Integer.parseInt(trueAnwsers);
            float coverQuiz = (float) (trueAns / total) * 100;
            if (coverQuiz > 0) {
                BigDecimal result;
                result = Utilities.round(coverQuiz, 1);
                cover = result.toString();
            }
        }
        
        String title = quiz.getTitle();
        request.setAttribute("title", title);
        
        request.setAttribute("s", s);
        request.setAttribute("question", question);

        request.setAttribute("totalQuestion", totalQuestion);
        request.setAttribute("trueAnwsers", trueAnwsers);
        request.setAttribute("cover", cover);

        request.setAttribute("questionid", questionid);
        request.setAttribute("numberOfRightOption", numberOfRightOption);
        request.setAttribute("quizid", quizid);
        request.getRequestDispatcher("Learn.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LearnController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LearnController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
