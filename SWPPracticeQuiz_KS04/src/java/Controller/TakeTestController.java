/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.TestDAO;
import Model.QuestionTest;
import Model.Test;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Array;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dangm
 */
@WebServlet(name = "TakeTestController", urlPatterns = {"/TakeTestController"})
public class TakeTestController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String s = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
            request.setAttribute("s", s);
            String testid = (String) request.getAttribute("testid");
            if (testid == null) {
                String testidd = request.getParameter("testid");
                String minutee = request.getParameter("minute");
                //System.out.println(minute);
                String mi[] = minutee.split(":");
//                String min[] = mi[1].split("");
//                String minuteee = mi[0]+":"+min[0]+(Integer.parseInt(min[1])-1);
                double minute = Double.parseDouble(mi[0])+Double.parseDouble(mi[1])/60-0.0045;
                
                TestDAO testDAO = new TestDAO();
                
                String questionid = request.getParameter("questionid");
                String currentQuestionid = request.getParameter("current_questionid");
                String checkOption[] = request.getParameterValues("Question" + currentQuestionid);
                testDAO.updateOptionStatus(testidd, currentQuestionid);
                if (checkOption != null) {
                    for (String checkOption1 : checkOption) {
                        //System.out.println(checkOption[i]);
                        testDAO.updateOptionStatusInTest(testidd, currentQuestionid, checkOption1);
                    }
                    testDAO.updateQuestionDone(testidd,currentQuestionid,true);
                }
                if (checkOption == null) {
                    testDAO.updateQuestionDone(testidd,currentQuestionid,false);
                }
                if (questionid == null) {
                    questionid = "1";
                }
                int question = Integer.parseInt(questionid) - 1;
//                System.out.println(question);
//                System.out.println(test.getListQuestionTest().get(1));
                Test test = testDAO.getTestDetails(testidd);
                for (int i = 0; i < test.getListQuestionTest().size(); i++) {
                    System.out.println(test.getListQuestionTest().get(i).isDone());
                }
                QuestionTest questionTest = test.getListQuestionTest().get(question);

                request.setAttribute("testid", testidd);
                request.setAttribute("minutee", minutee);
                request.setAttribute("minute", minute);
                request.setAttribute("test", test);
                request.setAttribute("questionid", questionid);
                request.setAttribute("questionTest", questionTest);
//            request.setAttribute("time", time);
                String submit = request.getParameter("submitAllAnser");
                if(submit.equals("")){
                    request.getRequestDispatcher("takeTest.jsp").forward(request, response);
                }else{
                    request.setAttribute("testid", testid);
                    request.getRequestDispatcher("SubmitTestController").forward(request, response);
                }
            } else {
                TestDAO testDAO = new TestDAO();
                Test test = testDAO.getTestDetails(testid);
                String questionid = request.getParameter("questionid");
//                System.out.println(questionid);
                if (questionid == null) {
                    questionid = "1";
                }
                int question = Integer.parseInt(questionid) - 1;
//                System.out.println(question);
//                System.out.println(test.getListQuestionTest().get(1));
                QuestionTest questionTest = test.getListQuestionTest().get(question);
                
                request.setAttribute("testid", testid);
                request.setAttribute("test", test);
                request.setAttribute("questionid", questionid);
                request.setAttribute("questionTest", questionTest);
//            request.setAttribute("time", time);
                request.getRequestDispatcher("takeTest.jsp").forward(request, response);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(TakeTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(TakeTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
