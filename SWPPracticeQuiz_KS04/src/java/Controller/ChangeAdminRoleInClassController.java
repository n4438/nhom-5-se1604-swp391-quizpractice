package Controller;

import DAO.ClassDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ChangeAdminRoleInClassController", urlPatterns = {"/ChangeAdminRoleInClassController"})
public class ChangeAdminRoleInClassController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String classid = request.getParameter("classid");
        String userid = request.getParameter("userid");
        String isAdmin = request.getParameter("isAdmin");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            request.setAttribute("message", "You need to login before change role of member in your class.");
        } else {
            ClassDAO classdao = new ClassDAO();
            boolean check = classdao.changeRoleOfMember(classid, userid, isAdmin);
            if (check) {
                request.setAttribute("messageDelete1", "Change role successfully.");
                request.getRequestDispatcher("ViewMemberInClassController").forward(request, response);
            } else {
                request.setAttribute("messageDelete2", "Fail to change, please do later.");
                request.getRequestDispatcher("ViewMemberInClassController").forward(request, response);
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ChangeAdminRoleInClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ChangeAdminRoleInClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
