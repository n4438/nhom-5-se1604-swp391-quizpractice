/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.QuizDAO;
import Model.Quiz_Viewer;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fptshop
 */
@WebServlet(name = "SearchQuizByAdminController", urlPatterns = {"/SearchQuizByAdminController"})
public class SearchQuizByAdminController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String txtSearch = request.getParameter("txt");
        QuizDAO dao = new QuizDAO();
        List<Quiz_Viewer> list = dao.searchQuizbyName(txtSearch);
        PrintWriter out = response.getWriter();
        for (Quiz_Viewer quiz : list) {
            out.println("<div class=\"table-item\">\n"
                    + "                                                <div class=\"thead\">\n"
                    + "                                                    <div style=\"color: black\" class=\"submissions\"><a href=\"#\">"+quiz.getTitle()+"</a></div>\n"
                    + "                                                    <div style=\"color: black\" class=\"total-subm\">"+quiz.getEmail()+"</div>\n"
                    + "                                                    <div style=\"color: black\" class=\"replied\"></div>\n"
                    + "                                                    <div style=\"color: black\" class=\"latest-reply\">"+quiz.getNumOfViewers()+"</div>\n"
                    + "                                                    <div class=\"toggle tb-icon\">\n"
                    + "                                                        <a href=\"#\"><i class=\"fa fa-angle-down\"></i></a>\n"
                    + "                                                    </div>\n"
                    + "                                                </div>\n"
                    + "\n"
                   
                    + "\n"
                    + "                                                </div>\n"
                    + "                                            </div>");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SearchQuizByAdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SearchQuizByAdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
