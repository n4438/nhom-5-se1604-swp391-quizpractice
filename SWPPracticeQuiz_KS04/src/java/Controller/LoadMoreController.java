/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RateAndFeedBackDAO;
import Model.RateAndFeedback;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fptshop
 */
@WebServlet(name = "LoadMoreController", urlPatterns = {"/LoadMoreController"})
public class LoadMoreController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String id = request.getParameter("quizid");
        String count = request.getParameter("count");
        PrintWriter out = response.getWriter();
        RateAndFeedBackDAO dao2 = new RateAndFeedBackDAO();
        List<RateAndFeedback> Top3RateAndFeedbacks = dao2.getTop3RateOfQuiz(Integer.parseInt(id), Integer.parseInt(count));
        int lastRateId = 0;
        for (RateAndFeedback i : Top3RateAndFeedbacks) {
            String active = String.valueOf(i.getRate());
            String none = String.valueOf(5-i.getRate());
            out.println("<li class=\"review comment\">\n"
                    + "                                    <div class=\"body-review\">\n"
                    + "                                        <div class=\"review-author\">\n"
                    + "                                            <a href=\"#\">\n");
                    if(i.getAvatar() == null){
                        out.println("<img style=\"background-color: white;\" src=\"imageUpload/default-avatar.png\" alt=\"\">\n");
                    }
                    else {
                        out.println("<img src=\"imageUpload/"+i.getAvatar()+"\" alt=\"\">\n");
                    }
                    out.println( "                                            </a>\n"
                    + "                                        </div>\n"
                    + "                                        <div class=\"content-review\">\n"
                    + "                                            <h3 class=\"sm black\">\n"
                    + "                                                <a href=\"#\">Username - "+i.getUsername()+"</a>\n"
                    + "                                            </h3>\n"
                    + "                                            <div class=\"rating\">\n");
                    for(int z = 1; z<=i.getRate(); z++){
                        out.println("<a href=\"#\" class=\"active\"></a>");
                    }
                    for(int t = 1; t <= 5-i.getRate(); t++){
                        out.println("<a href=\"#\"></a>");
                    }
                    out.println("</div>\n"
                    + "                                            <p>"+i.getFeedback()+ "</p>\n"
                    + "                                        </div>\n"
                    + "                                    </div>\n"
                    + "                                </li>");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoadMoreController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoadMoreController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
