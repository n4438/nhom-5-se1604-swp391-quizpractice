/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author dangm
 */
public class UserAdminDAO {

    public List<User> getUser() {
        List<User> list = new ArrayList<>();
//        QuizDAO dao = new QuizDAO();
        UserAdminDAO dao = new UserAdminDAO();
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select * from [User]"
                + " order by username asc";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User u = new User();
                int id = rs.getInt(1);
                String username = rs.getString(2);
                int role = rs.getInt(8);
                u.setRoleId(role);
                u.setId(id);
                u.setUserName(username);
                u.setCountQuizCreated(dao.getNumQuizOfUser(id));
                list.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<User> getUserByUsername(String username1) {
        List<User> list = new ArrayList<>();
//        QuizDAO dao = new QuizDAO();
        UserAdminDAO dao = new UserAdminDAO();
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select * from [User]where "
                + "username like ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + username1 + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User u = new User();
                int id = rs.getInt(1);
                String username = rs.getString(2);
                int role = rs.getInt(8);
                u.setRoleId(role);
                u.setId(id);
                u.setUserName(username);
                u.setCountQuizCreated(dao.getNumQuizOfUser(id));
                list.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public boolean banUser(String userid) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "   update [User]\n"
                    + "  set role_id = 10\n"
                    + "  where userid = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, userid);
            ps.execute();
            return true;
        } catch (Exception e) {
        }
        return false;
    }
        public boolean unBanUser(String userid) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "   update [User]\n"
                    + "  set role_id = 0\n"
                    + "  where userid = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, userid);
            ps.execute();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public int getNumQuizOfUser(int userId) {
        int num = 0;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select count(quizid) from Quiz\n"
                + "                    where userid=?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return num;
    }

    public int countUser() {
        int num = 0;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select count(userid) from [User]";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return num;
    }

    public int countQuiz() {
        int num = 0;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select count(quizid) from Quiz";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return num;
    }

    public int countCategory() {
        int num = 0;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select count(categoryid) from Category";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return num;
    }

    public int countClass() {
        int num = 0;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select count(classid) from Class";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return num;
    }

    public String numVisitorInWeek(LocalDate today) {
        UserAdminDAO admin = new UserAdminDAO();
        String numOfEachDay = "";
        List<LocalDate> list = new ArrayList<>();
        list.add(today);

        LocalDate temp = today;
        DayOfWeek tempday = temp.getDayOfWeek();
        while (tempday.getValue() != DayOfWeek.MONDAY.getValue()) {
            temp = temp.minusDays(1);
            list.add(temp);
            tempday = temp.getDayOfWeek();
        }

        temp = today;
        tempday = temp.getDayOfWeek();
        while (tempday.getValue() != DayOfWeek.SUNDAY.getValue()) {
            temp = temp.plus(1, ChronoUnit.DAYS);
            list.add(temp);
            tempday = temp.getDayOfWeek();
        }
        Collections.sort(list);
        int asdf = 1;
        for (LocalDate localDate : list) {
            String dateNow = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            asdf += 1;
            if (asdf <= 7) {
                numOfEachDay += admin.getNumVisitorOfday(dateNow) + " ,";
            } else {
                numOfEachDay += admin.getNumVisitorOfday(dateNow);
            }
//            System.out.println(dateNow+ " "+ admin.getNumVisitorOfday(dateNow) );
        }
        return numOfEachDay;
    }

    public int getNumVisitorOfday(String date) {
        int num = 0;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select numberOfView from Visitor\n"
                + "                    where [time] = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, date);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return num;
    }

    public boolean checkVisit(String date) {
        boolean check = false;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "select * from Visitor \n"
                + "  where [time] = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, date);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return check;
    }

    public void updateVisitor(String date) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = " update Visitor set numberOfView = numberOfView +1\n"
                + "                     where [time] = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, date);
            ps.execute();
        } catch (Exception e) {
        }
    }

    public void insertVisitor() {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "insert Visitor ([time], numberOfView)  values (GETDATE(),1)";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.execute();
        } catch (Exception e) {
        }
    }
}
