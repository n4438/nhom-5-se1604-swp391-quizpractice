package DAO;

import Model.Class_User;
import Model.Classes;
import Model.Quiz;
import Model.QuizUser;
import Validation.Utilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author fptshop
 */
public class ClassDAO {

    public String generateRandomCode() throws SQLException {
        String code = "";
        String s = "abcdefghijklmnopqrstuwxyzABCDYFGHIJKLMNOPQRSTUWXYZ1234567890";
        int charAt;
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            charAt = random.nextInt(60);
            code += s.charAt(charAt);
        }
        ClassDAO cd = new ClassDAO();
        ArrayList<String> list = cd.getListRandomCode();
        for (int i = 0; i < list.size(); i++) {
            if (code.equals(list.get(i))) {
                generateRandomCode();
            }
        }
        return code;
    }

    public ArrayList<String> getListRandomCode() throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<String> list = new ArrayList<String>();
        try {
            conn = new DBContext().connection;
            String query = "Select code from Class\n";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // count number of class of a user
    public int countClassOfUser(int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select count (*) from Class_User where userid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    public boolean addClass(int userid, String className, String code, boolean allow) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "INSERT INTO Class\n"
                    + "VALUES (?, ?, getDate(), ?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setString(2, className);
            ps.setString(3, code);
            ps.setBoolean(4, allow);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get Class_User of a class
    public List<Class_User> getClassUserOfAClass(int classid, String search) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<Class_User> list = new ArrayList<>();
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select c.*, u.*, y.username as UserAdd from Class_User c join [User] u \n"
                    + "                    on c.userid = u.userid left join [User] y\n"
                    + "					on c.joinBy = y.userid where classid = ? and u.username like ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setString(2, "%" +search+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Class_User classUser = new Class_User(classid, rs.getInt(2), rs.getDate(3),
                        rs.getBoolean(4), rs.getInt(5), rs.getString(7), rs.getString(14));
                list.add(classUser);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get last classid by user
    public int getLastClassByUser(int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "Select top 1 classid from Class where createid=? order by classid DESC";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    // count number of class of a user
    public int countMemberOfClass(int classid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select count (*) from Class_User where classid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // generate class code of a user tocreate class
    public String getRandomClassCodeByUser(int userid) throws SQLException {
        ClassDAO dao = new ClassDAO();
        int classExisted = dao.countClassOfUser(userid);
        String code = userid + "_" + (classExisted + 1);
        Utilities uti = new Utilities();
        String random = Utilities.generateRandomCode();
        code += random;
        if (dao.checkClassExistByCode(code)) {
            return dao.getRandomClassCodeByUser(userid);
        } else {
            return code;
        }

    }

    // get Classid by code
    public int getClassIdByCode(String code) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select classid from Class \n"
                    + "where code = ? ";
            ps = conn.prepareStatement(query);
            ps.setString(1, code);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // check class exist by code
    public boolean checkClassExistByCode(String code) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select count(*) from Class where code=?";
            ps = conn.prepareStatement(query);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1) != 0;
            }
            return false;
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // check class exist
    public boolean checkClassExistById(int classid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select count(*) from \n"
                    + "Class where classid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1) != 0;
            }
            return false;
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // join a class by code
    public boolean joinClassByCode(int classid, int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "insert into Class_User (classid, userid, dateJoin,\n"
                    + "isAdmin, joinBy) values (?,?,GETDATE(), 0 , 0)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setInt(2, userid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // join a class by code
    public boolean insertOwnClass_User(int classid, int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "insert into Class_User (classid, userid, dateJoin,\n"
                    + "isAdmin, joinBy) values (?,?,GETDATE(), 1 , ?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setInt(2, userid);
            ps.setInt(3, userid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // check user in class
    public boolean checkIfUserInClass(int classid, int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select count(*) from \n"
                    + "Class_User where classid = ? and userid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setInt(2, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1) != 0;
            }
            return false;
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // check user in class
    public boolean checkIfUserIsAdminInClass(int classid, int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select isAdmin from Class_User where classid = ? and userid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setInt(2, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1) != 0;
            }
            return false;
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get Class by classid
    public Classes getClassbyClassid(int classid) throws SQLException {
        String query = "select * from class where classid=?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Classes classodid = new Classes();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Classes(classid, rs.getInt(2), rs.getString(3), rs.getDate(4), rs.getString(5),
                        rs.getBoolean(6));
            }
            return null;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get all quiz in class
    public List<QuizUser> getAllQuizInClass(int classid, String search) throws SQLException {
        String query = "select u.username, q.*, count(e.quizid) from [user] u join quiz q\n"
                + "on u.userid = q.userid join Question e on q.quizid = e.quizid\n"
                + "group by q.quizid,u.username,q.classid,q.userid,q.title,q.[Description],\n"
                + "q.Date_Created,q.Last_update\n"
                + "having q.classid = ? and q.title like ? order by q.quizid desc";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<QuizUser> list = new ArrayList<>();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setString(2, "%"+search+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt(2), rs.getInt(3), rs.getString(1),
                        rs.getString(4),
                        rs.getString(5), rs.getDate(6), rs.getDate(7), rs.getInt(9));
                // quiz.setNumberOfQuestion();
                list.add(quiz);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get all quiz in class by user
    public List<QuizUser> getAllQuizInClassCreateByUserid(int classid, int userid, String search) throws SQLException {
        String query = "select u.username, q.*, count(e.quizid),u.userid from [user] u join quiz q\n"
                + "                on u.userid = q.userid join Question e on q.quizid = e.quizid\n"
                + "                group by q.quizid,u.username,q.classid,q.userid,q.title,q.[Description],\n"
                + "                q.Date_Created,q.Last_update,u.userid\n"
                + "                having q.classid = ? and u.userid = ? and q.title like ? order by q.quizid desc";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<QuizUser> list = new ArrayList<>();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, classid);
            ps.setInt(2, userid);
            ps.setString(3, "%"+search+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt(2), rs.getInt(3), rs.getString(1),
                        rs.getString(4),
                        rs.getString(5), rs.getDate(6), rs.getDate(7), rs.getInt(9));
                list.add(quiz);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // count number question of a quiz in class
    public int countNumberOfQuestion(int quizid) throws SQLException {
        String query = "select count(*) from question \n"
                + "where quizid = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // remove a class
    public boolean leaveClass(int userid, int classid) throws SQLException {
        String query = "delete from Class_User where userid = ? and classid = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, classid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    //
    // remove a class
    public boolean removeMemberInClass(String userid, String classid) throws SQLException {
        String query = "delete from Class_User where userid = ? and classid = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, userid);
            ps.setString(2, classid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get own class
    public List<Classes> getClassCreateByUser(int userid,String search) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<Classes> list = new ArrayList<>();
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select c.*, u.username, count(y.userid) \n"
                    + "from Class c left join [User] u\n"
                    + "on c.createid = u.userid \n"
                    + "left join Class_User y on c.classid = y.classid\n"
                    + "group by c.classid,c.createid,c.classname,\n"
                    + "c.DateCreated, c.code, c.allowOthercreateQuiz,u.username\n"
                    + "having c.createid=? and c.classname like ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
             ps.setString(2, "%"+search +"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Classes classs = new Classes(rs.getInt(1), userid, rs.getString(3), rs.getDate(4), rs.getString(5),
                        rs.getBoolean(6), rs.getString(7), rs.getInt(8));
                list.add(classs);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get class if user id admin
    public List<Classes> getClassIfUserIsAdmin(int userid, String search) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<Classes> list = new ArrayList<>();
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select c.classid, c.createid,c.classname,c.DateCreated,\n"
                    + "c.code,c.allowOthercreateQuiz,e.username from Class_User u\n"
                    + "join Class c on u.classid=c.classid join [user] e\n"
                    + "on c.createid = e.userid\n"
                    + "where u.userid = ? and c.createid not in (?)\n"
                    + "and u.isAdmin=1 and c.classname like ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, userid);
            ps.setString(3, "%"+search+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Classes classs = new Classes(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getDate(4),
                        rs.getString(5),
                        rs.getBoolean(6), rs.getString(7));
                list.add(classs);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    //
    // get class if user id admin
    public List<Classes> getClassIfUserIsMember(int userid, String search) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<Classes> list = new ArrayList<>();
            DBContext db = new DBContext();
            conn = db.connection;
            String query = "select c.classid, c.createid,c.classname,c.DateCreated,\n"
                    + "c.code,c.allowOthercreateQuiz,e.username from Class_User u\n"
                    + "join Class c on u.classid=c.classid join [user] e\n"
                    + "on c.createid = e.userid\n"
                    + "where u.userid = ? and c.classname like ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setString(2, "%"+search+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Classes classs = new Classes(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getDate(4),
                        rs.getString(5),
                        rs.getBoolean(6), rs.getString(7));
                list.add(classs);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // remove a class
    public boolean changeRoleOfMember(String classid, String userid, String isAdmin) throws SQLException {
        String query = "update Class_User \n"
                + "set isAdmin = null where classid=? and userid=?";
        if (isAdmin.equals("true")) {
            query = "update Class_User \n"
                    + "set isAdmin = 1 where classid=? and userid=?";
        } else if (isAdmin.equals("false")) {
            query = "update Class_User \n"
                    + "set isAdmin = 0where classid=? and userid=?";
        }
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, classid);
            ps.setString(2, userid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        ClassDAO dao = new ClassDAO();
        System.out.println(dao.deleteAClassById("4"));
        // System.out.println(dao.joinClassByCode(1, 7));
        // System.out.println(dao.getAllQuizInClass(1).size());
        // System.out.println(dao.getAllQuizInClassCreateByUserid(1, 2).size());
        // System.out.println(dao.getClassIfUserIsMember(2).get(0));
    }

    public boolean changeAllowCreateQuiz(String classid, String allow) throws SQLException {
        String query = "update Class set allowOthercreateQuiz = null\n"
                + "where classid=?";
        if (allow.equals("yes")) {
            query = "update Class set allowOthercreateQuiz = 1\n"
                    + "where classid=?";
        } else if (allow.equals("no")) {
            query = "update Class set allowOthercreateQuiz = 0\n"
                    + "where classid=?";
        }
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, classid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean checkUserIsCreator(String classid, int id) throws SQLException {
        String query = "select createid from Class\n"
                + "where classid=?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int createid = 0;
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, classid);
            rs = ps.executeQuery();
            while (rs.next()) {
                createid = rs.getInt(1);
            }
            if (id == createid) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // delete own class
    public boolean deleteQuizInClass(String classid) throws SQLException {
        ClassDAO dao = new ClassDAO();
        List<QuizUser> list = dao.getAllQuizInClass(Integer.parseInt(classid),"");
        QuizDAO qdao = new QuizDAO();
        for (int i = 0; i < 10; i++) {
            boolean check = qdao.deleteQuiz(list.get(i).getQuizid() + "");
            if (check == false) {
                return false;
            }
        }
        return true;
    }

    // delete own class
    public boolean deleteAClass(String classid) throws SQLException {
        String query = "delete from Class_User where classid = ?\n"
                + "delete from Class where classid = ?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, classid);
            ps.setString(2, classid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // delete own class
    public boolean deleteAClassById(String classid) throws SQLException {
        String query = "delete from [View] where quizid in (select quizid from Quiz where classid=?)\n"
                + "delete Rate where quizid in (select quizid from Quiz where classid=?)\n"
                + "delete [Option] where quizid in (select quizid from Quiz where classid=?)\n"
                + "delete Question where quizid in (select quizid from Quiz where classid=?)\n"
                + "delete category_Quiz where quizid in (select quizid from Quiz where classid=?)\n"
                + "delete from Option_test\n"
                + "where testid in (select testid from Test where quizid in (select quizid from Quiz where classid=?))\n"
                + "delete from Question_test\n"
                + "where testid in (select testid from Test where quizid in (select quizid from Quiz where classid=?))\n"
                + "delete from Test\n"
                + "where testid in (select testid from Test where quizid in (select quizid from Quiz where classid=?))\n"
                + "delete Quiz where quizid in (select quizid from Quiz where classid=?)\n"
                + "delete from Class_User where classid = ?\n"
                + "delete from Class where classid = ?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DBContext db = new DBContext();
            conn = db.connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, classid);
            ps.setString(2, classid);
            ps.setString(3, classid);
            ps.setString(4, classid);
            ps.setString(5, classid);
            ps.setString(6, classid);
            ps.setString(7, classid);
            ps.setString(8, classid);
            ps.setString(9, classid);
            ps.setString(10, classid);
            ps.setString(11, classid);
            
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
