/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Option;
import Model.OptionTest;
import Model.Question;
import Model.QuestionTest;
import Model.Test;
import Model.TestTitle;
import Model.Test_Load;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dangm
 */
public class TestDAO {

    public List<TestTitle> getAllTestByUserId(int userId) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TestTitle> list = new ArrayList<>();
        String query = "select distinct title, userid,quizid from test where userid = " + userId;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new TestTitle(rs.getString(1), rs.getInt(2), rs.getInt(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public List<TestTitle> getAllTestByUserIdInClass(String classid, int userId) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TestTitle> list = new ArrayList<>();
        String query = "select distinct title, userid,quizid from test where quizid in\n"
                + "(select quizid from quiz where classid=?) and userid = ?";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, classid);
            ps.setInt(2, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new TestTitle(rs.getString(1), rs.getInt(2), rs.getInt(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public List<Test_Load> getAllTestByQuizIdAndUserId(int quizId, int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Test_Load> list = new ArrayList<>();
        String query = "select * from test where quizid = " + quizId + " and userid = " + userid;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Test_Load(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5),
                        rs.getDate(6), rs.getDate(7), rs.getInt(8), rs.getFloat(9), rs.getString(10), rs.getInt(11)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public List<Question> getQuestionForTest(int quizid, int numOfQuiz) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Question> listQuesInTest = new ArrayList<>();
        String query = "select top (?) * from Question where quizid = ? order by newid()";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, numOfQuiz);
            ps.setInt(2, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Question quesTest = new Question();
                int questionid = rs.getInt(2);
                String questionTitle = rs.getString(3);
                String instruction = rs.getString(4);
                quesTest.setRandomQuestion(rs.getBoolean(5));
                List<Option> listOp = new ArrayList<>();
                if (rs.getBoolean(5) == true) {
                    listOp = new TestDAO().getOptionByQuestion(quizid, questionid, true);
                } else {
                    listOp = new TestDAO().getOptionByQuestion(quizid, questionid, false);
                }
                quesTest.setQuizid(quizid);
                quesTest.setIntruction(instruction);
                quesTest.setQuestionid(questionid);
                quesTest.setTitle(questionTitle);
                quesTest.setListOptions(listOp);

                listQuesInTest.add(quesTest);
            }
            for (int i = 0; i < listQuesInTest.size(); i++) {
                listQuesInTest.get(i).setQuestionid(i + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return listQuesInTest;
    }

    public List<Option> getOptionByQuestion(int quizid, int questionid, boolean check) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Option> listOp = new ArrayList<>();
        String query;
        if (check) {
            query = "select * from [Option] where quizid = ? and questionid = ? order by newid()";
        } else {
            query = "select * from [Option] where quizid = ? and questionid = ?";
        }
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            ps.setInt(2, questionid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Option op = new Option();
                int optionid = rs.getInt(3);
                String option_content = rs.getString(4);
                boolean right_option = rs.getBoolean(5);
                op.setOptionid(optionid);
                op.setContent(option_content);
                op.setRight_option(right_option);
                listOp.add(op);
            }
            for (int i = 0; i < listOp.size(); i++) {
                listOp.get(i).setOptionid(i + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return listOp;
    }

    public boolean CreateNewTest(int userid, int quizid, String title, String description, int totalNumberOfQuiz,
            double minute) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "insert into Test (userid,quizid,title,[Description],Date_Created,Last_update,NumberOfQuestion,[time]) values\n"
                    + "(?,?,?,?,GETDATE(),GETDATE(),?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, quizid);
            ps.setString(3, title);
            ps.setString(4, description);
            ps.setInt(5, totalNumberOfQuiz);
            ps.setDouble(6, minute);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getLastTestByUserID(int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            String query = "select top 1 * from Test where userid= ? order by testid desc";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public boolean insertNewQuestionTest(int questionid, int testid, String title, String intruction,
            int numberOfRightAnswer) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "insert into Question_test (questionid,testid,question_title,Intruction,numberOfRightAnswer,done) values\n"
                    + "(?,?,?,?,?,0)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, questionid);
            ps.setInt(2, testid);
            ps.setString(3, title);
            ps.setString(4, intruction);
            ps.setInt(5, numberOfRightAnswer);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // public int countNumberOfRightAnswer(QuestionTest question) {
    // int count = 0;
    // if (question.getList_optionTest() != null) {
    // for (int i = 0; i < question.getList_optionTest().size(); i++) {
    // if (question.getList_optionTest().get(i).isRightOption()) {
    // count++;
    // }
    // }
    // }
    // return count;
    // }
    public int countNumberOfRightAnswer(Question question) {
        int count = 0;
        if (question.getListOptions() != null) {
            for (int i = 0; i < question.getListOptions().size(); i++) {
                if (question.getListOptions().get(i).isRight_option()) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean insertNewOptionTest(int testid, int questionid, int optionid, String content, boolean rightOption)
            throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "insert into Option_test (testid,questionid,optionid,option_content,right_option) values\n"
                    + "(?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, testid);
            ps.setInt(2, questionid);
            ps.setInt(3, optionid);
            ps.setString(4, content);
            ps.setBoolean(5, rightOption);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Test getTestDetails(String testid) throws SQLException {
        String query = "select t.*, e.questionid, e.question_title, e.Intruction, o.optionid, o.option_content, o.right_option,e.numberOfRightAnswer,o.option_status,e.done,e.question_status from Test t\n"
                + "join Question_test e on t.testid = e.testid\n"
                + "join [Option_test] o on t.testid = o.testid and e.questionid = o.questionid\n"
                + "where t.testid=?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        QuizDAO dao = new QuizDAO();
        TestDAO testDAO = new TestDAO();

        try {
            Test test = new Test();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, testid);
            rs = ps.executeQuery();
            int count = 0;
            int question = -1;

            while (rs.next()) {
                if (count == 0) {
                    test.setTestid(rs.getInt(1));

                    test.setUserid(rs.getInt(2));
                    test.setQuizid(rs.getInt(3));
                    test.setTitle(rs.getString(4).trim());
                    test.setDecription(rs.getString(5).trim());
                    test.setDateCreated(rs.getDate(6));
                    test.setLastUpdate(rs.getDate(7));
                    test.setNumberOfQuestion(rs.getInt(8));
                    test.setTime(rs.getString(10));
                    count++;
                }
                if (rs.getInt(15) == 1) {
                    List<QuestionTest> list = test.getListQuestionTest();
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    question++;
                    list.add(new QuestionTest(rs.getInt(1), rs.getInt(12), rs.getString(13).trim(),
                            rs.getString(14).trim(), rs.getInt(18), rs.getBoolean("done"), rs.getBoolean("question_status")));
                    test.setListQuestionTest(list);
                }
                List<OptionTest> listO = test.getListQuestionTest().get(question).getList_optionTest();
                if (listO == null) {
                    listO = new ArrayList<>();
                }
                listO.add(new OptionTest(rs.getInt(1), rs.getInt(12), rs.getInt(15), rs.getString(16).trim(),
                        rs.getBoolean("right_option"), rs.getBoolean("option_status")));
                test.getListQuestionTest().get(question).setList_optionTest(listO);
            }
            return test;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    // update option status of test

    public void updateOptionStatusInTest(String testid, String questionid, String optionid) throws SQLException {
        String query = "update Option_test set option_status= 1 where  testid=? and questionid=? and optionid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, testid);
            ps.setString(2, questionid);
            ps.setString(3, optionid);
            ps.executeUpdate();
            // return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        // return false;
    }

    public void updateQuestionDone(String testid, String questionid, boolean done) throws SQLException {
        String query = "update Question_test set done = ? \n"
                + "where  testid=? \n"
                + "and questionid=? ";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setBoolean(1, done);
            ps.setString(2, testid);
            ps.setString(3, questionid);
            ps.executeUpdate();
            // return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // update submit test in question table
    public boolean updateQuestionForSubmitTest(String testid, String questionid, boolean question_status)
            throws SQLException {
        String query = "update Question_test set done = 1,question_status = ?\n"
                + "                where  testid=? and questionid=? ";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setBoolean(1, question_status);
            ps.setString(2, testid);
            ps.setString(3, questionid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    //
    // update submit test in question table
    public boolean updateTestForSubmitTest(float mark, float correctAnswer, String testid) throws SQLException {
        String query = "update Test set Last_update=getdate(),\n"
                + "				Mark=?, correctAnswer=? where testid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setFloat(1, mark);
            ps.setFloat(2, correctAnswer);
            ps.setString(3, testid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        TestDAO d = new TestDAO();
        List<Test_Load> list = d.getAllTestByQuizIdAndUserId(25, 2);
        for (Test_Load test_Load : list) {
            System.out.println(test_Load);
        }
    }

    public void updateOptionStatus(String testidd, String questionid) throws SQLException {
        String query = "update Option_test set option_status= 0 where  testid=? and questionid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, testidd);
            ps.setString(2, questionid);
            ps.executeUpdate();
            // return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Test getTestResult(Test test) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            String query = "select * from Test where testid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, test.getTestid());
            rs = ps.executeQuery();
            while (rs.next()) {
                test.setCorrectAnswer(rs.getInt("correctAnswer"));
                test.setLastUpdate(rs.getDate("last_update"));
                test.setMark(rs.getFloat("Mark"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return test;
    }

}
